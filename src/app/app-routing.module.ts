import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanillaComponent } from './recibo/planilla/planilla.component';
import { HomeComponent } from './recibo/home/home.component';
import { ProductComponent } from './recibo/product/product.component';
import { DatosComponent } from './recibo/datos/datos.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'product/:nit', component: ProductComponent },
  { path: 'datos/:operRec/:operFac', component: DatosComponent },
  { path: 'planilla', component: PlanillaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
