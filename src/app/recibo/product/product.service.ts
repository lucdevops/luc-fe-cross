import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Prods } from '../../model/prods';

@Injectable()
export class ProdsService {

    private crearCP: string = 'http://192.168.1.81:8080/contapyme/crear';
    private datosCP: string = 'http://192.168.1.81:8080/contapyme/datos';

    private crearWMS: string = 'http://192.168.1.81:8081/wms/crear';
    private datosWMS: string = 'http://192.168.1.81:8081/wms/datos';


    constructor(private http: HttpClient) { }

    public crearRecepcion(prods: Prods, nit: string) {

        let separador = '';
        let producto = '';

        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < prods.items.length; i++) {

            if (producto === '') {
                separador = '';
            } else {
                separador = ',';
            }

            // tslint:disable-next-line: max-line-length
            producto = producto + separador + '{\"iinventario\":\"1101\",\"irecurso\":\"' + prods.items[i].producto + '\",\"itiporec\":\"\",\"qrecurso\":\"' + prods.items[i].cantidad + '\",\"icc\":\"\",\"sobserv\":\"\"}';
        }

        var url = this.crearCP + '/crearRecepcion/' + producto + '/2/1101/805013107/' + nit;
        console.log(url);
        return this.http.get(url);
    }

    public infoTercero(nit: string ) {
        return this.http.get(this.datosWMS + '/infoTercero/' + nit);
    }

    public precioRef(lista: string, productos: string, nit: string) {
        return this.http.get(this.datosCP + '/referencia/' + lista + '/' + nit + '/1101/' + productos);
    }

    public crearFactura(vendedor: string, nit: string, productos: string, valorNeto: string, lista: string) {
        // tslint:disable-next-line: max-line-length
        return this.http.get(this.crearCP + '/crearFactura/' + productos + '/2/17/1101/' + nit + '/' + vendedor + '/' + valorNeto + '/' + lista);
    }

}
