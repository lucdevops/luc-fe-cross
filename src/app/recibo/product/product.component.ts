import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { Prods } from '../../model/prods';
import { ProdsService } from './product.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  prods: Prods[];
  prod: number;
  pointIndex = 0;
  contador = 0;
  cnt: number;
  result: any = '';
  miFormulario: FormGroup;
  closeResult: string;
  nit: any;
  focus: any;
  numRecep: string;
  lista: string;
  public isShowingSecond: boolean;
  public loading: boolean;

  constructor(
    private fb: FormBuilder,
    private prodService: ProdsService,
    private activatedRoute: ActivatedRoute) {

    this.focus = '';
    this.isShowingSecond = false;
    this.nit = this.activatedRoute.snapshot.paramMap.get('nit');
  }

  ngOnInit() {
    this.miFormulario = this.fb.group({
      items: this.fb.array([this.fb.group({
        cantidad: ['', Validators.required],
        producto: ['', [Validators.required, Validators.maxLength(13), Validators.minLength(13)]]
      })])
    });
  }

  submit(formValue: any) {
    this.contador++;
    let separador = '';
    let productos = '';
    const prods = new Prods();
    prods.items = formValue.items;

    if (this.miFormulario.valid) {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < prods.items.length; i++) {
        if (productos === '') {
          separador = '';
        } else {
          separador = '';
        }
        productos = productos + separador + '{' + prods.items[i].producto + '-' + prods.items[i].cantidad + '}';
      }
      this.loading = true;

      this.prodService.crearRecepcion(prods, this.nit).subscribe(result => {
        this.prodService.infoTercero(this.nit).subscribe(info => {
          this.prodService.precioRef(info[0], productos, this.nit).subscribe(data => {
            this.prodService.crearFactura(info[1], this.nit, data[0], data[1], info[0]).subscribe(respuesta => {
              if (result[0] === 'true' && respuesta[0] === 'true') {
                window.location.href = '/datos/' + result[1] + '/' + respuesta[1];
              }
            });
          });
        });
      });
    }
  }

  get getItems() {
    return this.miFormulario.get('items') as FormArray;
  }

  addItem() {
    const control = <FormArray>this.miFormulario.controls['items'];
    control.push(this.fb.group({ cantidad: [], producto: [] }));
    this.pointIndex++;
  }

  removeItem(index: number) {
    const control = <FormArray>this.miFormulario.controls['items'];
    control.removeAt(index);
    this.pointIndex--;
  }

  public setFocus(fieldToFocus: string): void {
    this.focus = fieldToFocus;
  }

  public toggleSecond(fieldToFocus: string): void {
    this.isShowingSecond = !this.isShowingSecond;
    this.setFocus(fieldToFocus);
  }

  onInputEntry(event, nextInput) {
    let input = event.target;
    let length = input.value.length;
    let maxLength = input.attributes.maxlength.value;

    if (length >= maxLength) {
      nextInput.focus();
    }
  }
}
