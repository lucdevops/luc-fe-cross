import { Component, OnInit, ɵConsole } from '@angular/core';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { PlanillaService } from './planilla.service';
import { Marca } from 'src/app/model/marca';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, flatMap } from 'rxjs/operators';
import { Cliente } from 'src/app/model/clientes';


@Component({
  selector: 'app-planilla',
  templateUrl: './planilla.component.html',
  styleUrls: ['./planilla.component.css']
})
export class PlanillaComponent implements OnInit {

  model: NgbDateStruct;
  miFormulario: FormGroup;
  marca: Marca[];
  myControl = new FormControl();
  terceros: Cliente[];
  filteredref: Observable<Cliente[]>;
  agendados: string[] = [];
  title = '';
  value = '';
  proveedor: string;
  result: any;

  constructor(private calendar: NgbCalendar, private planillaService: PlanillaService) { }

  ngOnInit() {

    this.model = this.calendar.getToday();

    this.planillaService.cargarMar().subscribe(marca => {
      this.marca = marca;
    });

    this.planillaService.cargarCliDia(this.model.day).subscribe(ter => {
      this.terceros = ter;
    });

    this.filteredref = this.myControl.valueChanges
      .pipe(
        map(value => typeof value === 'string' ? value : value.descripcion),
        flatMap(value => value ? this.filterref(value) : [])
      );
  }

  selectToday() {
    this.model = this.calendar.getToday();
  }

  filterref(value: string): Observable<Cliente[]> {
    const filterValue = value.toLowerCase();
    return this.planillaService.cargaCli(filterValue);
  }

  submit(formValue: any) {
    console.log(formValue);
  }

  agregar(data: string) {
    this.agendados.push(data);
  }

  quitar(data) {
    this.agendados = this.agendados.filter(s => s !== data);
  }

  guardar() {
    this.planillaService.crearPedidoWMS(this.proveedor, this.agendados).subscribe(error => {
      this.result = error;
      this.agendados = [];

    });
  }

  addCli(newAgen: string) {
    if (newAgen) {
      this.agendados.push(newAgen);
    }
  }

}
