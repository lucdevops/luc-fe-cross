import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Marca } from 'src/app/model/marca';
import { Observable } from 'rxjs';
import { Cliente } from 'src/app/model/clientes';

@Injectable({
  providedIn: 'root'
})
export class PlanillaService {

  private crearCP: string = 'http://192.168.1.81:8080/contapyme/crear';
  private datosCP: string = 'http://192.168.1.81:8080/contapyme/datos';

  private crearWMS: string = 'http://192.168.1.81:8081/wms/crear';
  private datosWMS: string = 'http://192.168.1.81:8081/wms/datos';

  constructor(private http: HttpClient) { }

  cargarMar(): Observable<Marca[]> {
    return this.http.get<Marca[]>(this.datosWMS + '/cargaMar');
  }

  cargaCli(descripcion): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.datosWMS + '/cargaCli/' + descripcion);
  }

  cargarCliDia(dia): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.datosWMS + '/cargaCliDia/' + dia);
  }

  crearPedidoWMS(proveedor, agendados) {
    return this.http.get(this.crearWMS + '/crearPedidoWMS/' + agendados + '/' + proveedor);
  }


}
