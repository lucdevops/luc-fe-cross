import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DemoMaterialModule } from './material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PlanillaComponent } from './recibo/planilla/planilla.component';
import { HomeComponent } from './recibo/home/home.component';
import { ProductComponent } from './recibo/product/product.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AutofocusDirective } from './directives/autofocus.directive';
import { HttpClientModule } from '@angular/common/http';
import { DatosComponent } from './recibo/datos/datos.component';
import { ProdsService } from './recibo/product/product.service';

const routes: Routes = [];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    PlanillaComponent,
    HomeComponent,
    ProductComponent,
    DatosComponent,
    AutofocusDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule
  ],
  providers: [ProdsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
